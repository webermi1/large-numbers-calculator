# Large Numbers Calculator

Tento projekt je semestrální práce na předmět PJC. Je to kalkulačka pro počítání s velkými čísly, které se nevejdou do standarních datových typů. Kalkulačka podporuje operace sčítání, odčítání a násobení. Pro sčítání a násobení jsou použity běžné naivní algoritmy. Pro násobení je použit Karacubův algoritmus.

Kalkulačka podporuje operace s desetinnými i zápornými čísly.

# Příklady použití

Návod k použití:

`
./large_numbers_calculator --help
`

Spuštění kalkulačky:
`
./large_numbers_calculator --c
`

Po spuštění je možné zadávat jednoduché výrazy jako např.
`
1348484858584*484848484
`
`
287447474.2444-23344455544
`
`
-4848484*(-484848)
`

- v jednom výrazu je vždycky podporovaná jenom jedna binární operace, složitější výrazy nejsou podporované
- desetinná čísla musí být oddělená desetinnou tečkou
- pokud zadáváte záporné číslo a není na začátku, musí být uzávorkované
- řetězec by neměl obsahovat žádné bílé znaky

