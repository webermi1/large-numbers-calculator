#ifndef LARGE_NUMBERS_CALCULATOR_CLARGE_H
#define LARGE_NUMBERS_CALCULATOR_CLARGE_H

#include <string>
#include <memory>
#include <vector>

using namespace std;

class CLarge {
public:
    CLarge();

    /**
     * Creates Clarge number.
     * @param num number
     * @param decPoint decimal point index
     * @param sign sign
     */
    CLarge(const vector<unique_ptr<short int>> &num, const size_t decPoint, bool sign = true);

    /**
     * Copy constructor
     * @param CLarge number
     */
    CLarge(const CLarge &x);

    /**
     * Gets CLarge as vec.
     * @return clarge
     */
    vector<unique_ptr<short int>> getValue() const;


    /**
     * Method to get the part before the decimal point.
     * @return part of CLarge in vector.
     */
    vector<unique_ptr<short int>> getDecimalPart() const;


    /**
     * Method to get fractional part of CLarge in vector.
     * @return decimal part of CLarge in vector
     */
    vector<unique_ptr<short int>> getWholePart() const;

    /**
     * Gets the decimal point of the number.
     * @return decimal point
     */
    size_t getDecPoint() const;

    /**
     * Gets sign of the number.
     * @return sign
     */
    bool getSign() const;

    /**
     * Converts the number into string.
     * @return number in the string
     */
    string toString() const;


    /**
     * Changes sign of the number.
     */
    void changeSign();

    /**
     * Overloaded plus operator.
     */
    CLarge operator+(const CLarge &x) const;


    /**
     * Overloaded minus operator.
     */
    CLarge operator-(const CLarge &x) const;

    /**
     * Overloaded - operator.
     */
    CLarge operator*(const CLarge &x) const;


    /**
     * Substract the whole parts of numbers.
     * @param left left vec
     * @param right right vec
     * @param carry carry
     * @return
     */
    static vector<unique_ptr<short int>> subtractWholePart(const vector<unique_ptr<short int>> &left,
                                                           const vector<unique_ptr<short int>> &right,
                                                           bool &carry);

    /**
     * Resolves sign of multiplication.
     * @param left sign 1
     * @param right sign 2
     * @return result sign
     */
    bool signMultiplication(const bool left, const bool right) const;

    /**
     * Karatsuba algorithm for multiplication.
     * @param left left op
     * @param right right op
     * @return result
     */
    vector<unique_ptr<short int>> KaratsubaAlg(const vector<unique_ptr<short int>> &left,
                                               const vector<unique_ptr<short int>> &right) const;

    /**
     * Splits vector based on pos.
     * @param src src vector
     * @param l lower part
     * @param h higher part
     * @param pos pos
     */
    void splitVec(const vector<unique_ptr<short int>> &src, vector<unique_ptr<short int>> &l,
                  vector<unique_ptr<short int>> &h, const size_t pos) const;

    /**
     * Gets 10^exponent in vector
     * @param exponent
     * @return result
     */
    vector<unique_ptr<short int>> powerOfTen(const size_t exponent) const;

    /**
     * Simplifies subtraction.
     * @param left left
     * @param right right
     * @param resultSign result sign
     * @return type of substraction
     */
    int simplifySubtract(const bool left, const bool right, bool &resultSign) const;


    /**
     * Compares two vectors
     * @param left
     * @param right
     * @return comparation result
     */
    int compareWholeParts(const vector<unique_ptr<short int>> &left, const vector<unique_ptr<short int>> &right) const;

    /**
     * Compares the decimal parts
     * @param left
     * @param right
     * @return comparation result
     */
    int compareDecimalParts(const vector<unique_ptr<short int>> &left,
                            const vector<unique_ptr<short int>> &right) const;

    /**
     * Adds two whole part of the numbers.
     * @param left left
     * @param right right
     * @param carry carry
     * @return result
     */
    vector<unique_ptr<short int>> addWholePart(const vector<unique_ptr<short int>> &left,
                                               const vector<unique_ptr<short int>> &right,
                                               bool &carry) const;

    /**
     * Multiplies two vectors.
     * @param left  left
     * @param right right
     * @return result
     */
    vector<unique_ptr<short int>> multiply(const vector<unique_ptr<short int>> &left,
                                           const vector<unique_ptr<short int>> &right) const;

    /**
     * If it is carry, adds 1 at the beginning.
     * @param carry carry
     * @param vec result
     */
    void isCarry(bool &carry, vector<unique_ptr<short int>> &vec) const;

    /**
     * Subtracts two decimal parts.
     * @param left left
     * @param right right
     * @param carry carry
     * @return result
     */
    vector<unique_ptr<short int>> subtractDecimalPart(const vector<unique_ptr<short int>> &left,
                                                      const vector<unique_ptr<short int>> &right,
                                                      bool &carry) const;

    /**
     * Adds two decimal parts.
     * @param left left
     * @param right right
     * @param carry carry
     * @return result
     */
    vector<unique_ptr<short int>> addDecimalPart(const vector<unique_ptr<short int>> &left,
                                                 const vector<unique_ptr<short int>> &right,
                                                 bool &carry) const;

    /**
     * Finds out if the vec is zero or not.
     * @param vec vec
     * @return is zero
     */
    bool zeroVec(vector<unique_ptr<short int>> &vec) const;

private:
    vector<unique_ptr<short int>> m_num;
    size_t m_decPoint;
    bool m_sign;
};


#endif //LARGE_NUMBERS_CALCULATOR_CLARGE_H
