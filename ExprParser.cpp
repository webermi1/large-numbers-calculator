#include <algorithm>
#include <stdexcept>
#include <memory>
#include "ExprParser.h"

bool isOperator(const char &c) {
    return c == '+' || c == '-' || c == '*' || c == '/';
}


void ExprParser::parseExpr(CLarge &left_op, CLarge &right_op, Operation &op, const std::string &expr) {
    bool first = false;
    bool second = false;
    bool ope = false;
    std::string tmp_expr = expr;

    for (int i = 0; i != tmp_expr.size(); ++i) {
        if (!first) {
            left_op = readNum(tmp_expr, i);
            first = true;
            continue;
        }

        if (!ope) {
            if (!isOperator(tmp_expr[i])) {
                throw std::invalid_argument("operator expected");
            }
            ope = true;
            op = Operation(static_cast<Operation>(tmp_expr[i]));
            continue;
        }

        if (!second) {
            right_op = readNum(tmp_expr, i);
            return;
        }
    }
}

bool isNumber(const char &c) {
    return c >= '0' && c <= '9';
}

CLarge ExprParser::readNum(const std::string &expr, int &index) {
    bool sign = true;

    if (expr[index] == '(') {
        ++index;
    }

    if (index < expr.size() && expr[index] == '-') {
        sign = false;
        ++index;
    }

    std::vector<std::unique_ptr<short int>> number;
    bool is_float = false;
    int float_index = -1;
    int num_index = 0;
    while (index != expr.size()) {
        if (isNumber(expr[index])) {
            number.push_back(std::make_unique<short int>(expr[index] - '0'));
            ++num_index;
        } else if (expr[index] == '.' && !is_float) {
            is_float = true;
            float_index = num_index;
        } else if (expr[index] == '.' && is_float) {
            throw std::invalid_argument("unexpected character in the expr");
        } else if (expr[index] == ')') {
            break;
        } else {
            --index;
            break;
        }
        ++index;
    }

    if (float_index == num_index) {
        throw std::invalid_argument("dot found at the end of the expression");
    }

    return CLarge(number, float_index, sign);
}
