#ifndef LARGE_NUMBERS_CALCULATOR_OUTPUTWRITER_H
#define LARGE_NUMBERS_CALCULATOR_OUTPUTWRITER_H


#include <string>

/**
 * Class which writes to the output.
 */
class OutputWriter {
private:
    static const std::string SEE_HELP_MS;
    static const std::string HELP_MS;
    static const std::string CALCULATE_MS;
    static const std::string CONTINUE_MS;

    OutputWriter() = default;

public:
    /**
     * Prints --help message to the std output.
     */
    static void printHelpMsg();

    /**
     * Prints error which occurred during reading the program arguments.
     * @param msg error message
     */
    static void printInvalidArgMsg(const char *msg);

    /**
     * Prints message witch the instruction for starting the calculation.
     */
    static void printStartCalculationMsg();

    /**
     * After evaluating the first expression, program asks if the user wants to continue with the next expr.
     */
    static void printContinueMessage();
};


#endif //LARGE_NUMBERS_CALCULATOR_OUTPUTWRITER_H
