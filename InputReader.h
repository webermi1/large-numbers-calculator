#ifndef LARGE_NUMBERS_CALCULATOR_INPUTREADER_H
#define LARGE_NUMBERS_CALCULATOR_INPUTREADER_H


#include <string>

/**
 * Static class which reads the inputs from CLI.
 */
class InputReader {
public:

    /**
     * Enum which holds program supported arguments.
     */
    enum Argument {HELP, CALCULATOR};

    /**
     * Resolves program arguments and returns the operation which should be performed.
     * @param argc number of arguments
     * @param argv arguments
     * @return operation to perform
     */
    static Argument resolveProgramArguments(int argc, char *argv[]);

    /**
     * Reads line from the standard input.
     * @return line
     */
    static std::string readLine();

private:
    InputReader() = default;
};


#endif //LARGE_NUMBERS_CALCULATOR_INPUTREADER_H
