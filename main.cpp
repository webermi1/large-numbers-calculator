#include <iostream>
#include <stdexcept>
#include "InputReader.h"
#include "ExprParser.h"
#include "OutputWriter.h"
#include "CLarge.h"

int main(int argc, char *argv[]) {

    InputReader::Argument argument = InputReader::resolveProgramArguments(argc, argv);

    if(argument == InputReader::HELP) {
        OutputWriter::printHelpMsg();
        return 0;
    }

    bool next = true;
    OutputWriter::printStartCalculationMsg();

    while(next)
    {
        // Read expression
        std::string line = InputReader::readLine();

        // Parse expression
        CLarge left, right, res;
        Operation operation;
        ExprParser parser = ExprParser();
        parser.parseExpr(left, right, operation, line);

        if(operation == Operation::MINUS) {
            res = left - right;
        }
        else if(operation == Operation::PLUS) {
            res = left + right;
        }
        else if(operation == Operation::MULTIPLY) {
            res = left * right;
        }
        // Calculate
        std::cout << "result" << std::endl;
        std::cout << res.toString() << std::endl;

        // Do you want to continue?
        OutputWriter::printContinueMessage();
        line = InputReader::readLine();
        if(line == "n") next = false;
        else if(line == "y") continue;
        else throw std::invalid_argument("unexpected character");
    }

    return 0;
}
