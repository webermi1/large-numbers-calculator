#ifndef LARGE_NUMBERS_CALCULATOR_EXPRPARSER_H
#define LARGE_NUMBERS_CALCULATOR_EXPRPARSER_H

#include <string>
#include "CLarge.h"

/**
 * Supported operations
 */
enum class Operation : char {
    PLUS = '+',
    MINUS = '-',
    MULTIPLY = '*',
};

class ExprParser {
public:
    /**
     * Method for parsing the expression.
     * @param left_op left operand
     * @param right_op right operand
     * @param op operation to be performed
     * @param expr expression from which should be the information obtained
     */
    void parseExpr(CLarge &left_op, CLarge &right_op, Operation &op, const std::string &expr);
private:
    CLarge readNum(const std::string &expr, int &index);
};

#endif //LARGE_NUMBERS_CALCULATOR_EXPRPARSER_H
