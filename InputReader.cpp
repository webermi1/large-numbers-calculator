#include "InputReader.h"
#include <stdexcept>
#include <cstring>
#include <iostream>


InputReader::Argument InputReader::resolveProgramArguments(int argc, char **argv) {
    if (argc != 2) {
        throw std::invalid_argument("invalid number of arguments");
    }

    char *arg = argv[1];

    if (strcmp(arg, "--help") == 0) {
        return InputReader::HELP;
    } else if (strcmp(arg, "--c") == 0) {
        return InputReader::CALCULATOR;
    } else {
        throw std::invalid_argument("invalid program argument specified");
    }
}

std::string InputReader::readLine() {
    std::string line;
    getline(std::cin, line);
    return line;
}
