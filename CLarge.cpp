#include "CLarge.h"

CLarge::CLarge(const vector<unique_ptr<short int>> &num, const size_t decPoint, bool sign) {
    m_sign = sign;
    m_decPoint = decPoint;

    vector<unique_ptr<short int>> tmp;
    for (size_t i = 0; i != num.size(); ++i) tmp.push_back(make_unique<short int>(*(num[i])));

    if (tmp.size() > 1) {
        while (*(tmp[0]) == 0) {
            tmp.erase(tmp.begin());
            if (m_decPoint != 0) --m_decPoint;
            if (tmp.size() == 1) break;
        }

        if (m_decPoint == 0 && decPoint != 0) tmp.insert(tmp.begin(), make_unique<short int>(0));
    }

    if (m_decPoint != 0) {
        while (*(tmp[tmp.size() - 1]) == 0) {
            tmp.erase(tmp.end() - 1);
            if (tmp.size() == 1) break;
        }

        if (m_decPoint == tmp.size()) m_decPoint = 0;
    }

    for (size_t i = 0; i < tmp.size(); ++i) m_num.push_back(make_unique<short int>(*(tmp[i])));
}

CLarge::CLarge(const CLarge &x) {
    m_sign = x.getSign();
    m_decPoint = x.getDecPoint();
    m_num = x.getValue();
}


string CLarge::toString() const {
    string result;

    if (!m_sign) result.push_back('-');

    for (size_t i = 0; i != m_num.size(); ++i) {
        if ((i == m_decPoint) && (i != 0)) result.push_back('.');
        result.push_back((*(m_num[i])) + '0');
    }
    return result;
}

vector<unique_ptr<short int>> CLarge::getValue() const {
    vector<unique_ptr<short int>> num;
    for (size_t i = 0; i < m_num.size(); ++i) num.push_back(make_unique<short int>(*(m_num[i])));
    return num;
}

size_t CLarge::getDecPoint() const {
    return m_decPoint;
}

vector<unique_ptr<short int>> CLarge::getWholePart() const {
    vector<unique_ptr<short int>> x;
    if (m_decPoint != 0) for (size_t i = 0; i < m_decPoint; ++i) x.push_back(make_unique<short int>(*(m_num[i])));
    else for (size_t i = 0; i < m_num.size(); ++i) x.push_back(make_unique<short int>(*(m_num[i])));
    return x;
}

vector<unique_ptr<short int>> CLarge::getDecimalPart() const {
    vector<unique_ptr<short int>> x;
    if (m_decPoint != 0)
        for (size_t i = m_decPoint; i < m_num.size(); ++i)
            x.push_back(make_unique<short int>(*(m_num[i])));
    return x;
}

bool CLarge::getSign() const {
    return m_sign;
}

void CLarge::changeSign() {
    m_sign = !m_sign;
}


CLarge CLarge::operator+(const CLarge &x) const {
    bool sign, carry = 0;
    int type = simplifySubtract(getSign(), x.getSign(), sign);

    if (type == 4) {
        CLarge tmp = (*this);
        tmp.changeSign();
        CLarge result = tmp - x;
        if (!sign) result.changeSign();
        return result;
    }

    if (type == 3) {
        CLarge tmp = x;
        tmp.changeSign();
        CLarge result = (*this) - tmp;
        if (!sign) result.changeSign();
        return result;
    }

    bool flag = 0;
    vector<unique_ptr<short int>> result = addDecimalPart(getDecimalPart(), x.getDecimalPart(), carry);
    if (zeroVec(result)) {
        result.clear();
        flag = 1;
    }

    vector<unique_ptr<short int>> whole = addWholePart(getWholePart(), x.getWholePart(), carry);
    if (carry) whole.insert(whole.begin(), make_unique<short int>(1));

    for (size_t i = 0; i < whole.size(); ++i) result.insert(result.begin() + i, make_unique<short int>(*(whole[i])));

    if (!flag) return CLarge(result, whole.size(), sign);
    return CLarge(result, 0, sign);
}

CLarge CLarge::operator-(const CLarge &x) const {
    bool sign, carry = 0;
    int comp_whole, comp_dec, type = simplifySubtract(getSign(), x.getSign(), sign);

    if (type == 4) {
        CLarge tmp = (*this);
        tmp.changeSign();
        CLarge result = tmp + x;
        if (!sign) result.changeSign();
        return result;
    }

    if (type == 3) {
        CLarge tmp = x;
        tmp.changeSign();
        CLarge result = (*this) + tmp;
        if (!sign) result.changeSign();
        return result;
    }

    CLarge res;
    vector<unique_ptr<short int>> result;
    comp_whole = compareWholeParts(getWholePart(), x.getWholePart());
    comp_dec = compareDecimalParts(getDecimalPart(), x.getDecimalPart());

    if (comp_whole == 0) {
        if (comp_dec == 1) {
            result = subtractDecimalPart(getDecimalPart(), x.getDecimalPart(), carry);
            result.insert(result.begin(), make_unique<short int>(0));
            return CLarge(result, 1, sign);
        } else {
            result = subtractDecimalPart(x.getDecimalPart(), getDecimalPart(), carry);
            result.insert(result.begin(), make_unique<short int>(0));
            return CLarge(result, 1, !sign);
        }
    } else if (comp_whole == 1) {
        bool flag = 0;
        result = subtractDecimalPart(getDecimalPart(), x.getDecimalPart(), carry);
        if (zeroVec(result)) {
            result.clear();
            flag = 1;
        }

        vector<unique_ptr<short int>> sub = subtractWholePart(getWholePart(), x.getWholePart(), carry);
        if (carry) sub.insert(sub.begin(), make_unique<short int>(1));
        for (size_t i = 0; i < sub.size(); ++i) result.insert(result.begin() + i, make_unique<short int>(*(sub[i])));

        if (!flag) return CLarge(result, sub.size(), sign);
        return CLarge(result, 0, sign);
    } else {
        res = x - (*this);
        res.changeSign();
    }

    return res;
}

CLarge CLarge::operator*(const CLarge &x) const {
    vector<unique_ptr<short int>> result = KaratsubaAlg(getValue(), x.getValue());
    bool sign = signMultiplication(getSign(), x.getSign());
    size_t decPoint = result.size() - (x.getDecimalPart().size() + getDecimalPart().size());
    return CLarge(result, decPoint, sign);
}

vector<unique_ptr<short int>>
CLarge::subtractWholePart(const vector<unique_ptr<short>> &left, const vector<unique_ptr<short>> &right,
                          bool &carry) {
    size_t j = right.size();
    short int num;
    vector<unique_ptr<short int>> result;

    for (size_t i = (left.size() - 1); i >= 0; --i) {
        if (j) {
            num = *(right[j - 1]);
            --j;
        } else num = 0;

        if ((num + carry) > (*(left[i]))) {
            result.insert(result.begin(), make_unique<short int>(((*(left[i]) + 10) - (num + carry))));
            carry = true;
        } else {
            result.insert(result.begin(), make_unique<short int>((*(left[i]) - (num + carry))));
            carry = false;
        }

        if (i == 0) break;
    }

    while (*(result[0]) == 0) {
        result.erase(result.begin());
        if (result.size() == 1) break;
    }

    return result;
}

bool CLarge::signMultiplication(const bool left, const bool right) const {
    return (left == right) ? true : false;
}

vector<unique_ptr<short int>>
CLarge::KaratsubaAlg(const vector<unique_ptr<short>> &left, const vector<unique_ptr<short>> &right) const {
    if (left.size() == 1 || right.size() == 1) {
        return multiply(left, right);
    }

    size_t n, m = max(left.size(), right.size());

    if (m % 2) {
        n = m / 2;
        ++n;
    } else n = m / 2;

    vector<unique_ptr<short int>> h1, h2, l1, l2;
    splitVec(left, l1, h1, n);
    splitVec(right, l2, h2, n);
    vector<unique_ptr<short int>> z2 = multiply(h1, h2);
    vector<unique_ptr<short int>> z0 = multiply(l1, l2);

    bool carry = 0;
    vector<unique_ptr<short int>> z1_1 = addWholePart(l1, h1, carry);
    isCarry(carry, z1_1);
    vector<unique_ptr<short int>> z1_2 = addWholePart(l2, h2, carry);
    isCarry(carry, z1_2);
    vector<unique_ptr<short int>> z1 = subtractWholePart(subtractWholePart(multiply(z1_1, z1_2), z2, carry), z0,
                                                         carry);
    z1_1.clear();
    z1_2.clear();

    //Return value
    vector<unique_ptr<short int>> v = powerOfTen(2 * n);
    vector<unique_ptr<short int>> v1 = multiply(z2, v);

    v.clear();
    v = powerOfTen(n);
    vector<unique_ptr<short int>> v2 = multiply(z1, v);
    v.clear();

    v = addWholePart(v1, v2, carry);
    isCarry(carry, v);
    v = addWholePart(v, z0, carry);
    isCarry(carry, v);
    return v;
}

void CLarge::splitVec(const vector<unique_ptr<short>> &src, vector<unique_ptr<short>> &l, vector<unique_ptr<short>> &h,
                      const size_t pos) const {
    if (src.size() <= pos) {
        for (size_t i = 0; i < src.size(); ++i) l.push_back(make_unique<short int>(*(src[i])));
        h.push_back(make_unique<short int>(0));
    } else {
        size_t i = 0;
        while ((src.size() - h.size()) != pos) {
            h.insert(h.begin() + i, make_unique<short int>(*(src[i])));
            ++i;
        }

        for (size_t j = i; j < src.size(); ++j) l.push_back(make_unique<short int>(*(src[j])));
    }
}

vector<unique_ptr<short int>> CLarge::powerOfTen(const size_t exponent) const {
    vector<unique_ptr<short int>> res;

    res.push_back(make_unique<short int>(1));
    for (size_t i = 0; i < exponent; ++i) {
        res.push_back(make_unique<short int>(0));
    }

    return res;
}

int CLarge::simplifySubtract(const bool left, const bool right, bool &resultSign) const {
    if (left && right) {
        resultSign = true;
        return 1;
    } else if (!left && !right) {
        resultSign = false;
        return 2;
    } else if (left && !right) {
        resultSign = true;
        return 3;
    } else {
        resultSign = false;
        return 4;
    }
}

int CLarge::compareWholeParts(const vector<unique_ptr<short>> &left, const vector<unique_ptr<short>> &right) const {
    if (left.size() > right.size()) return 1;

    if (left.size() < right.size()) return 2;

    for (size_t i = 0; i != left.size(); ++i) {
        if (*(left[i]) != *(right[i])) return *(left[i]) > *(right[i]) ? 1 : 2;
    }

    return 0;
}

int CLarge::compareDecimalParts(const vector<unique_ptr<short>> &left, const vector<unique_ptr<short>> &right) const {
    if (!left.size() && !right.size()) return 0;
    if (!left.size()) return 2;
    if (!right.size()) return 1;

    for (size_t i = 0; (i != left.size() || i != right.size()); ++i) {
        if (*(left[i]) != *(right[i])) return *(left[i]) > *(right[i]) ? 1 : 2;
    }

    if (left.size() != right.size()) return left.size() > right.size() ? 1 : 2;

    return 0;
}

vector<unique_ptr<short int>>
CLarge::addWholePart(const vector<unique_ptr<short>> &left, const vector<unique_ptr<short>> &right, bool &carry) const {
    if (left.size() < right.size()) return addWholePart(right, left, carry);

    vector<unique_ptr<short int>> result;
    size_t num, n = right.size();

    for (size_t i = (left.size() - 1); i >= 0; --i) {
        if (n > 0) {
            num = *(right[n - 1]);
            --n;
        } else num = 0;

        short int sum = *(left[i]) + carry + num;
        if (sum >= 10) {
            result.insert(result.begin(), make_unique<short int>(sum - 10));
            carry = true;
        } else {
            result.insert(result.begin(), make_unique<short int>(sum));
            carry = false;
        }

        if (i == 0) break;
    }

    return result;
}

vector<unique_ptr<short int>>
CLarge::multiply(const vector<unique_ptr<short>> &left, const vector<unique_ptr<short>> &right) const {
    if (left.size() < right.size()) {
        return multiply(right, left);
    }

    vector<unique_ptr<short int>> result, tmp;
    bool carry = false;
    size_t hold = 0;

    for (size_t i = (right.size() - 1); i >= 0; --i) {
        for (size_t n = 0; n != (right.size() - 1 - i); ++n) {
            tmp.insert(tmp.begin(), make_unique<short int>(0));
        }

        for (size_t j = (left.size() - 1); j >= 0; --j) {
            short int num = ((*(right[i])) * (*(left[j]))) + hold;
            if (num < 10) {
                tmp.insert(tmp.begin(), make_unique<short int>(num));
                hold = 0;
            } else {
                tmp.insert(tmp.begin(), make_unique<short int>(num % 10));
                num = num / 10;
                hold = num;
            }
            if (j == 0) break;
        }

        if (hold != 0) {
            tmp.insert(tmp.begin(), make_unique<short int>(hold));
            hold = 0;
        }

        result = addWholePart(tmp, result, carry);
        tmp.clear();
        if (carry) result.insert(result.begin(), make_unique<short int>(1));
        carry = false;
        if (i == 0) break;
    }

    return result;
}

void CLarge::isCarry(bool &carry, vector<unique_ptr<short>> &vec) const {
    if (carry) {
        vec.insert(vec.begin(), make_unique<short int>(1));
    }
    carry = 0;
}

vector<unique_ptr<short int>>
CLarge::subtractDecimalPart(const vector<unique_ptr<short>> &left, const vector<unique_ptr<short>> &right,
                            bool &carry) const {
    vector<unique_ptr<short int>> left_tmp, right_tmp;

    if (!left.size() && !right.size()) return left_tmp;
    for (size_t i = 0; i < left.size(); ++i) left_tmp.push_back(make_unique<short int>(*(left[i])));
    for (size_t i = 0; i < right.size(); ++i) right_tmp.push_back(make_unique<short int>(*(right[i])));


    while (left_tmp.size() < right_tmp.size()) left_tmp.push_back(make_unique<short int>(0));
    while (left_tmp.size() != right_tmp.size()) right_tmp.push_back(make_unique<short int>(0));

    vector<unique_ptr<short int>> result = subtractWholePart(left_tmp, right_tmp, carry);
    return result;
}

vector<unique_ptr<short int>>
CLarge::addDecimalPart(const vector<unique_ptr<short>> &left, const vector<unique_ptr<short>> &right,
                       bool &carry) const {
    vector<unique_ptr<short int>> result;

    if (left.size() == 0 && right.size() == 0) return result;
    if (left.size() == 0 && right.size() != 0) {
        for (size_t i = 0; i < right.size(); ++i) result.push_back(make_unique<short int>(*(right[i])));
        return result;
    }

    if (left.size() != 0 && right.size() == 0) {
        for (size_t i = 0; i < left.size(); ++i) result.push_back(make_unique<short int>(*(left[i])));
        return result;
    }

    if (left.size() < right.size()) return addDecimalPart(right, left, carry);

    if (left.size() > right.size()) {
        for (size_t i = right.size(); i < left.size(); ++i) result.push_back(make_unique<short int>(*(left[i])));
    }

    vector<unique_ptr<short int>> left_tmp;
    for (size_t i = 0; i < right.size(); ++i) left_tmp.push_back(make_unique<short int>(*(left[i])));

    vector<unique_ptr<short int>> add = addWholePart(left_tmp, right, carry);
    for (size_t i = 0; i < add.size(); ++i) result.insert(result.begin() + i, make_unique<short int>(*(add[i])));
    return result;
}

bool CLarge::zeroVec(vector<unique_ptr<short>> &vec) const {
    size_t i;

    for (i = 0; i != vec.size(); ++i) if (*(vec[i]) != 0) break;

    if (i != vec.size()) return 0;
    return 1;
}

CLarge::CLarge() {

}


