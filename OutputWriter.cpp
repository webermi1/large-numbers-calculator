#include <iostream>
#include "OutputWriter.h"

const std::string OutputWriter::SEE_HELP_MS = "Try --help for more information.";

const std::string OutputWriter::HELP_MS = "Welcome to the arbitrary precision calculator used for computing "
                                          "expression which does not fit into the standard data types.\n Usage: \n"
                                          " --c\truns shell for computing the expressions \n  --help\tprints usage "
                                          "information";

const std::string OutputWriter::CALCULATE_MS = "Please, enter your expression to be evaluated and press enter:";

const std::string OutputWriter::CONTINUE_MS = "Do you wish to continue with the next expression? [y/n]";


void OutputWriter::printHelpMsg() {
    std::cout << HELP_MS << std::endl;
}

void OutputWriter::printInvalidArgMsg(const char *msg) {
    std::cerr << "Error '" << msg << "' occurred while reading the program arguments" << std::endl << SEE_HELP_MS
              << std::endl;
}

void OutputWriter::printStartCalculationMsg() {
    std::cout << CALCULATE_MS << std::endl;
}

void OutputWriter::printContinueMessage() {
    std::cout << CONTINUE_MS << std::endl;
}

